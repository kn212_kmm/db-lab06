create procedure FindByTeamPosition @team nvarchar(50), @position nvarchar(50) as

select registration_id, surname, [dbo].[personal_cards].[name], birth_year, id_code,dbo.positions.name, dbo.unit.name salary, enrollment_date from dbo.personal_cards
inner join dbo.unit on dbo.unit.unit_id = dbo.personal_cards.unit_id
inner join dbo.positions on dbo.personal_cards.position_id = dbo.positions.position_id
where [dbo].[positions].[name] = @position and [dbo].[unit].[name] = @team

create procedure SalaryToMax @salary int as

Select surname, salary from dbo.personal_cards
inner join dbo.positions on dbo.personal_cards.position_id = dbo.positions.position_id
inner join dbo.unit on dbo.unit.unit_id = dbo.personal_cards.unit_id
where salary < @salary
order by salary

create procedure DocumentDidntExpiredByType @type nvarchar(50) as

select document_id, start_date, end_date, registration_id, [name] from dbo.documents
inner join dbo.document_types on dbo.document_types.type_id =dbo.documents.type_id
where dbo.document_types.name = @type and end_date < GETDATE()

create trigger DeleteEmpyGroup on documents for insert AS
if exists (select end_date from inserted where end_date > GETDATE())
begin 
	Rollback transaction print 'Your document expired'
end

create trigger DeleteEmptyTeam on personal_cards for DELETE AS
if not exists (select * from deleted join personal_cards on personal_cards.unit_id = deleted.unit_id)
begin 
	delete unit where unit.unit_id = (select unit_id from deleted)
end

create trigger UpdateSalary on personal_cards for insert AS
begin
	update positions
	set salary = 10000/(select COUNT(personal_cards.registration_id) from inserted join personal_cards on personal_cards.position_id = inserted.position_id)
	where positions.position_id = (select position_id from inserted)
end
