
create procedure AvgReiting as
update Reiting set Reiting = 91 where Kod_student=48
select Name_ini, avg(Reiting) as [Avg] from Reiting
join (select distinct max(K_zapis) over (partition by Name_ini, predmet.K_predmet) as [last], Name_ini
		from Rozklad_pids
		join dbo_groups on dbo_groups.Kod_group = Rozklad_pids.Kod_group
		join dbo_student on dbo_student.Kod_group = dbo_groups.Kod_group
		join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		join predmet on predmet.K_predmet = Predmet_plan.K_predmet) maxStud on [last] = K_zapis
group by Name_ini
Order by Name_ini

create procedure AvgReitingRetakes as

select Name_ini, avg(Reiting) as [Avg] from Reiting
join dbo_student on Kod_stud = Kod_student
join Rozklad_pids on Reiting.K_zapis = Rozklad_pids.K_zapis
join Predmet_plan on Rozklad_pids.K_predm_pl = Predmet_plan.K_predm_pl
join predmet on Predmet_plan.K_predmet = predmet.K_predmet
group by Name_ini
Order by Name_ini

create procedure FourFiveReiting as

Select * from(
Select Name_ini, [Avg], case
when [Avg]>=90 then '5'
when [Avg]>=72 then '4'
end mark 
from (
select Name_ini, avg(Reiting) as [Avg] from Reiting
join (select distinct max(K_zapis) over (partition by Name_ini, predmet.K_predmet) as [last], Name_ini
		from Rozklad_pids
		join dbo_groups on dbo_groups.Kod_group = Rozklad_pids.Kod_group
		join dbo_student on dbo_student.Kod_group = dbo_groups.Kod_group
		join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		join predmet on predmet.K_predmet = Predmet_plan.K_predmet) maxStud on [last] = K_zapis
group by Name_ini
) as marks) m
where m.mark is not null
Order by Name_ini

create procedure NationalEcts as

Select * from(
select Name_ini, [Avg],
	case 
		when [Avg]  < 60 then '2'
		when [Avg]  < 72 then '3'
		when [Avg]  < 90 then '4'
		when [Avg]  <= 100 then '5'
	end [National],
	case
		when [Avg]  < 35 then 'F'
		when [Avg]  < 50 then 'FX'
		when [Avg]  < 60 then 'E'
		when [Avg]  < 72 then 'D'
		when [Avg]  < 84 then 'C'
		when [Avg]  < 90  then 'B'
		when [Avg]  <= 100 then 'A'
	end ECTS
	from (
select Name_ini, avg(Reiting) as [Avg] from Reiting
join (select distinct max(K_zapis) over (partition by Name_ini, predmet.K_predmet) as [last], Name_ini
		from Rozklad_pids
		join dbo_groups on dbo_groups.Kod_group = Rozklad_pids.Kod_group
		join dbo_student on dbo_student.Kod_group = dbo_groups.Kod_group
		join Predmet_plan on Predmet_plan.K_predm_pl = Rozklad_pids.K_predm_pl
		join predmet on predmet.K_predmet = Predmet_plan.K_predmet) maxStud on [last] = K_zapis
group by Name_ini
) as marks) m
Order by Name_ini

create trigger StudentGroup on dbo_student FOR insert as
if not exists(
	select * from inserted 
	join dbo_groups on inserted.Kod_group = dbo_groups.Kod_group)
begin 
	Insert into dbo_groups (Kod_group, Kod_men, Kod_zhurn, K_navch_plan, kilk) values ((select Kod_group from inserted), 2, 3, 13, 19)
end

create trigger DeleteEmpyGroup on dbo_student FOR DELETE as
if not exists(
	select * from dbo_student
	join deleted on dbo_student.Kod_group = deleted.Kod_group)
begin 
	delete dbo_groups where dbo_groups.Kod_group = (select Kod_group from deleted)
end



